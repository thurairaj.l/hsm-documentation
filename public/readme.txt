Any issues about this product that were discovered too late to include in the documentation are described in the Customer Release Notes document (CRN).

If you are unable to find information that you need, contact Gemalto Technical Support (https://supportportal.gemalto.com)

NOTE ABOUT BROWSERS

The Help for Luna EFT is being successfully viewed with these browsers:

 - Firefox
 - Internet Explorer
 - Chrome


The documentation is compiled using Madcap Flare and any comments/references to MadCap software in the output are remnant code. Hence, these can be disregarded.
Gemalto 2018